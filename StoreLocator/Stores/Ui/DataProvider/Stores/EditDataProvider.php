<?php

namespace StoreLocator\Stores\UI\DataProvider\Stores;
use StoreLocator\Stores\Model\WorkingHoursRepository;
use StoreLocator\Stores\Model\ResourceModel\Stores\CollectionFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Magento\Framework\UrlInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Filesystem\Driver\File\Mime;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\ReadInterface;


class EditDataProvider extends AbstractDataProvider
{
    private ReadInterface $mediaDirectory;
    private WorkingHoursRepository $workingHoursRepository;
    public function __construct(
        $name, $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        private StoreManagerInterface $storeManager,
        WorkingHoursRepository $workingHoursRepository,
        Filesystem $filesystem,
        private Mime $mime,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $meta,
            $data
        );
        $this->mediaDirectory = $filesystem->getDirectoryRead(DirectoryList::MEDIA);
        $this->workingHoursRepository = $workingHoursRepository;
    }

    public function getDataSourseData(){
        return [];
    }

    public function getData()
    {
        $a = parent::getData();

        if(!isset($a['items'][0]['store_image'])){
            return $a;
        }
        // поправить параметр фото в Save (file / name)
        $imgDir = 'test';
        $baseUrl = $this->storeManager->getStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA);
        $imgname = $a['items'][0]['store_image'];
        $fullImagePath = $this->mediaDirectory->getAbsolutePath($imgDir) . '/' . $a['items'][0]['store_image'];
        $imageUrl = $baseUrl . $imgDir . '/' . $a['items'][0]['store_image'];
        $stat = $this->mediaDirectory->stat($fullImagePath);
        $a['items'][0]['store_image'] = null;
        $a['items'][0]['store_image'][0]['url'] = $imageUrl;
        $a['items'][0]['store_image'][0]['name'] =$imgname;
        $a['items'][0]['store_image'][0]['size'] = $stat['size'];
        $a['items'][0]['store_image'][0]['type'] = $this->mime->getMimeType($fullImagePath);

        $b = $this->workingHoursRepository->getByStoreId($a['items'][0]['store_id']);

        foreach ($b as $workhour){
            $a['items'][0]['workhours'][$workhour['working_days']]['end_time'] = $workhour['work_hours_to'];
            $a['items'][0]['workhours'][$workhour['working_days']]['start_time'] = $workhour['work_hours_from'];
            $a['items'][0]['workhours'][$workhour['working_days']]['working_hours_id'] = $workhour['working_hours_id'];
        }
//            echo '<pre>';
//        var_dump($a);dd();

        return $a;


    }

    /**
     * @return array
     */
    public function getMeta(): array
    {
        return $this->meta;
    }
}
