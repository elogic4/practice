<?php

namespace StoreLocator\Stores\Ui\Component\Control\Store;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\UrlInterface;
use phpDocumentor\Reflection\Utils;
use StoreLocator\Stores\Model\StoresRepository;
class GenericButton
{
    private UrlInterface $urlBuilder;
    private RequestInterface $request;
    private StoresRepository $storesRepository;
    public function __construct(
        UrlInterface $urlBuilder,
        RequestInterface $request,
        StoresRepository $storesRepository
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
        $this->storesRepository = $storesRepository;
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->urlBuilder->getUrl($route, $params);
    }

    public function getStoreId()
    {
        $storeId = $this->request->getParam('id');
        if ($storeId === null) {
            return 0;
        }
        $store = $this->storesRepository->get($storeId);

        return $store->getId() ?: null;
    }

}
