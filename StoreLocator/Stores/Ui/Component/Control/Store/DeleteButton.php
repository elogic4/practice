<?php

namespace StoreLocator\Stores\Ui\Component\Control\Store;
use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use StoreLocator\Stores\Ui\Component\Control\Store\GenericButton;
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    public function getButtonData()
    {
        if ($this->getStoreId()) {
            return [
                'id' => 'delete',
                'label' => __('Delete'),
                'on_click' => "deleteConfirm('" .__('Are you sure you want to delete this store?') ."', '"
                    . $this->getUrl('*/*/delete', ['id' => $this->getStoreId()]) . "', {data: {}})",
                'class' => 'delete',
                'sort_order' => 10
            ];
        }
        return [];
    }

}
