<?php

namespace StoreLocator\Stores\Api\Data;

interface StoreLocatorInterface
{
    const STORE_ID = 'store_id';
    const STORE_NAME = 'store_name';
    const STORE_INFO = 'store_info';
    const STORE_IMAGE = 'store_image';
    const ADDRESS = 'address';
    const SCHEDULE_WORK = 'schedule_work';
    const LONGITUDE = 'longitude';
    const LATITUDE = 'latitude';
    /**
     * @return mixed|null
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id);


//    public function getStoreId(): int;
//
//    /**
//     * @param int $storeId
//     */
//    public function setStoreId(int $storeId): void;

    /**
     * @return string
     */
   public function getStoreName(): string;

    /**
     * @param string $storeName
     * @return void
     */
    public function setStoreName(string $storeName): void;

    /**
     * @return string|null
     */
    public function getStoreInfo(): ?string;

//    public function setStoreInfo(string $storeInfo): void;
////
    /**
     * @return string|null
     */
    public function getStoreImage(): ?string;

    /**
     * @param string $storeImage
     * @return void
     */
    public function setStoreImage(string $storeImage): void;

    /**
     * @return string|null
     */
    public function getAddress(): ?string;

    /**
     * @param string $address
     * @return void
     */
    public function setAddress(string $address): void;

    /**
     * @return string|null
     */
//    public function getScheduleWork(): ?string;

    /**
     * @param string $scheduleWork
     * @return void
     */
    public function setScheduleWork(string $scheduleWork): void;

    /**
     * @return float|null
     */
   public function getLongitude(): ?float;

    /**
     * @param float $longitude
     * @return void
     */
    public function setLongitude(float $longitude): void;

    /**
     * @return float|null
     */
    public function getLatitude(): ?float;

    /**
     * @param float $latitude
     * @return void
     */
    public function setLatitude(float $latitude): void;

}
