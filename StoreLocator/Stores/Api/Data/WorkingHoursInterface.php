<?php

namespace StoreLocator\Stores\Api\Data;

interface WorkingHoursInterface
{
    const WORKING_HOURS_ID = "working_hours_id";
    const STORE_ID = "store_id";
    const WORKING_DAYS = "working_days";
    const WORK_HOURS_FROM = "work_hours_from";
    const WORK_HOURS_TO = "work_hours_to";
    public function getWorkingHoursId(): int;

    /**
     * @param int $workingHoursId
     * @return void
     */
    public function setWorkingHoursId(int $workingHoursId): void;

    /**
     * @return int
     */
    public function getStoreId(): int;

    /**
     * @param int $storeId
     * @return void
     */
    public function setStoreId(int $storeId): void;

    /**
     * @return int
     */
    public function getWorkingDays(): int;

    /**
     * @param int $workingDays
     * @return void
     */
    public function setWorkingDays(int $workingDays): void;

    /**
     * @return int
     */
    public function getWorkHoursFrom(): int;

    /**
     * @param int $workHoursFrom
     * @return void
     */
    public function setWorkHoursFrom(int $workHoursFrom): void;

    /**
     * @return int
     */
    public function getWorkHoursTo(): int;

    /**
     * @param int $workHoursTo
     * @return void
     */
    public function setWorkHoursTo(int $workHoursTo): void;
}
