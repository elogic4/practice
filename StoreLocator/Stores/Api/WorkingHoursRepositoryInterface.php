<?php

namespace StoreLocator\Stores\Api;

use StoreLocator\Stores\Api\Data\WorkingHoursInterface;

interface WorkingHoursRepositoryInterface
{

//    public function getById(int $workingHoursId): WorkingHoursInterface;
//
//
//    public function save(WorkingHoursInterface $workingHours): WorkingHoursInterface;
//
//
//    public function delete(WorkingHoursInterface $workingHours): void;


    public function getByStoreId(int $storeId): array;
}
