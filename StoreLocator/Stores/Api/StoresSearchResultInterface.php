<?php

namespace StoreLocator\Stores\Api;





interface StoresSearchResultInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * @return \StoreLocator\Stores\Api\Data\StoreLocatorInterface[]
     */
    public function getItems();
    /**
     * @param \StoreLocator\Stores\Api\Data\StoreLocatorInterface[]
     * @return void
     */
    public function setItems(array $items);
}
