<?php

namespace StoreLocator\Stores\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use StoreLocator\Stores\Api\Data\StoreLocatorInterface;

interface StoresRepositoryInterface
{
    /**
     * @param int $id
     * @return StoreLocatorInterface
     */
    public function get(int $id): StoreLocatorInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StoresSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): StoresSearchResultInterface;

    /**
     * @param StoreLocatorInterface $stores
     * @return StoreLocatorInterface
     */
    public function save(StoreLocatorInterface $stores): StoreLocatorInterface;

    /**
     * @param StoreLocatorInterface $stores
     * @return bool
     */
    public function delete(StoreLocatorInterface $stores): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool;

}
