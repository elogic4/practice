<?php

namespace StoreLocator\Stores\Controller\Adminhtml\Allstores;

use StoreLocator\Stores\Model\StoresRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\Data\SourceInterface;
use StoreLocator\Stores\Api\Data\StoreLocatorInterface;

class NewAction extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Backend::system';
    private StoresRepository $storesRepository;

    public function __construct(
        Context                $context,
        StoresRepository $storesRepository
    )
    {
        parent::__construct($context);
        $this->storesRepository = $storesRepository;
    }


    public function execute(): ResultInterface
    {
        $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $result->setActiveMenu('StoreLocator_Stores::store_locator')
            ->addBreadcrumb(__('Edit Store'), __('Store'));
        $result->getConfig()->getTitle()->prepend(__('New Store'));


        return $result;
    }
}
