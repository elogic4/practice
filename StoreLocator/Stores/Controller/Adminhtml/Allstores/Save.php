<?php

namespace StoreLocator\Stores\Controller\Adminhtml\Allstores;

use Magento\Framework\App\Config\ScopeConfigInterface;
use StoreLocator\Stores\Api\StoresRepositoryInterface;
use StoreLocator\Stores\Api\WorkingHoursRepositoryInterface;
use StoreLocator\Stores\Model\StoresFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use StoreLocator\Stores\Api\Data\WorkingHoursInterface;
use StoreLocator\Stores\Api\Data\StoreLocatorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\Data\SourceInterface;
use StoreLocator\Stores\Model\WorkingHoursFactory;

class Save extends Action implements HttpPostActionInterface
{
    private StoresRepositoryInterface $storesRepository;
    private StoresFactory $storesFactory;
    private WorkingHoursRepositoryInterface $workingHoursRepository;

    private WorkingHoursFactory $workingHoursFactory;
    private ScopeConfigInterface $scopeConfig;

    public function __construct(
        Context $context,
        StoresRepositoryInterface $storesRepository,
        StoresFactory $storesFactory,
        WorkingHoursRepositoryInterface $workingHoursRepository,
        WorkingHoursFactory $workingHoursFactory,
        ScopeConfigInterface $scopeConfig,
    ) {
        parent::__construct($context);
        $this->storesRepository = $storesRepository;
        $this->storesFactory = $storesFactory;
        $this->workingHoursRepository = $workingHoursRepository;
        $this->workingHoursFactory = $workingHoursFactory;
        $this->scopeConfig = $scopeConfig;
    }

    public function geocoder(string $address)
    {

        $apiKey = $this->scopeConfig->getValue('stores/general/google_maps_api_key',\Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($address) . "&key=" . $apiKey;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        if ($response === false) {
            $error = curl_error($ch);
            curl_close($ch);
            throw new \Exception("cURL error: " . $error);
        }

        curl_close($ch);

        $data = json_decode($response, true);


//        if ($data === null || !isset($data['status']) || $data['status'] !== 'OK') {
//            throw new \Exception("Invalid response from Google Geocoding API");
//        }



        return $data;
    }
    public function execute(): ResultInterface
    {

        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $requestData = $request->getPost()->toArray();

        if (!$request->isPost() || empty($requestData['general'])) {
            $this->messageManager->addErrorMessage(__('Wrong request.'));
            $resultRedirect->setPath('*/*/new');
            return $resultRedirect;
        }

        try {

            $id = $requestData['general'][StoreLocatorInterface::STORE_ID];
            $store = $this->storesRepository->get($id);
        } catch (\Exception $e) {
            $store = $this->storesFactory->create();
        }



        $store->setStoreName($requestData['general'][StoreLocatorInterface::STORE_NAME]);
        $store->setStoreInfo($requestData['general'][StoreLocatorInterface::STORE_INFO]);
        $store->setAddress($requestData['general'][StoreLocatorInterface::ADDRESS]);
        $location = $this->geocoder($requestData['general'][StoreLocatorInterface::ADDRESS]);
        try {
            $location = $location['results'][0]['geometry']['location'];
        }catch (\Exception $exception) {

            $this->messageManager->addErrorMessage(__('Error. Enter valid address. '));

            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }
        $latitude = $location['lat'];
        $longitude = $location['lng'];
        try{
            $store->setLatitude($latitude);
            $store->setLongitude($longitude);
        } catch (\Throwable  $e){

            $store->setLatitude(0);
            $store->setLongitude(0);
        }

//        $store->setScheduleWork($requestData['general'][StoreLocatorInterface::SCHEDULE_WORK]);
        if (!isset($requestData['general'][StoreLocatorInterface::STORE_IMAGE])) {
            $store->setStoreImage("default.jpg");
        } else {
            $storeImage = $requestData['general'][StoreLocatorInterface::STORE_IMAGE][0]["file"] ??
                $requestData['general'][StoreLocatorInterface::STORE_IMAGE][0]["name"];
            $store->setStoreImage($storeImage);
        }


        try {
            $store = $this->storesRepository->save($store);
            $this->processRedirectAfterSuccessSave($resultRedirect, $store->getId());
            $this->messageManager->addSuccessMessage(__('Store was saved.'));

        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage(__('Error. Cannot save. %1', $exception->getMessage()));

            $resultRedirect->setPath('*/*/new');
        }
        //saving workhours
        $i = -1;
        foreach ($requestData['general']['workhours'] as $workhour){
            $i++;
            if (!empty($workhour['start_time']) and !empty($workhour['end_time'])){
                if (!empty($workhour[WorkingHoursInterface::WORKING_HOURS_ID])){
                    $workid = (int)$workhour[WorkingHoursInterface::WORKING_HOURS_ID];
                    $workinghour = $this->workingHoursRepository->get($workid);
                    $workinghour->setStoreId($store->getId());
                    $workinghour->setWorkHoursFrom($workhour['start_time']);
                    $workinghour->setWorkHoursTo($workhour['end_time']);
                    $workinghour->setWorkingDays($i);
                    $workinghour=$this->workingHoursRepository->save($workinghour);
                } else {
                    $workinghour = $this->workingHoursFactory->create();
                    $workinghour->setStoreId($store->getId());
                    $workinghour->setWorkHoursFrom($workhour['start_time']);
                    $workinghour->setWorkHoursTo($workhour['end_time']);
                    $workinghour->setWorkingDays($i);
                    $workinghour=$this->workingHoursRepository->save($workinghour);
                }

            }

        }

        return $resultRedirect;
    }

    private function processRedirectAfterSuccessSave(Redirect $resultRedirect, string $id)
    {
        if ($this->getRequest()->getParam('back')) {
            $resultRedirect->setPath(
                '*/*/edit',
                [
                    StoreLocatorInterface::STORE_ID => $id,
                    '_current' => true,
                ]
            );
        } elseif ($this->getRequest()->getParam('redirect_to_new')) {
            $resultRedirect->setPath(
                '*/*/new',
                [
                    '_current' => true,
                ]
            );
        } else {
            $resultRedirect->setPath('*/*/');
        }
    }
}
