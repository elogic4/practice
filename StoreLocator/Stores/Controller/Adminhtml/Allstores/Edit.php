<?php

namespace StoreLocator\Stores\Controller\Adminhtml\Allstores;

use StoreLocator\Stores\Model\StoresRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\InventoryApi\Api\Data\SourceInterface;
use StoreLocator\Stores\Api\Data\StoreLocatorInterface;

class Edit extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Backend::system';
    private StoresRepository $storesRepository;

    public function __construct(
        Context                $context,
        StoresRepository $storesRepository
    )
    {
        parent::__construct($context);
        $this->storesRepository = $storesRepository;
    }


    public function execute(): ResultInterface
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $storeName = $this->storesRepository->get($id);

            /** @var Page $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $result->setActiveMenu('StoreLocator_Stores::store_locator')
                ->addBreadcrumb(__('Edit Store'), __('Store'));
            $result->getConfig()
                ->getTitle()
                ->prepend(__('Edit store: %storeName', ['storeName' => $storeName->getStoreName()]));
        } catch (NoSuchEntityException $e) {
            /** @var Redirect $result */
            $result = $this->resultRedirectFactory->create();
            $this->messageManager->addErrorMessage(
                __('Store with id "%value" does not exist.', ['value' => $id])
            );
            $result->setPath('*/*');
        }

        return $result;
    }
}
