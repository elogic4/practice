<?php

namespace StoreLocator\Stores\Controller\Adminhtml\Allstores;


use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    const ADMIN_RESOURCE = 'Magento_Backend::system';

    private $resultPageFactory;
    private $scopeConfig;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->scopeConfig = $scopeConfig;
    }

    public function execute(): ResultInterface
    {
        $isEnabled = $this->scopeConfig->getValue('stores/general/enable_module', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        if (!$isEnabled){
            $resultRedirect = $this->resultRedirectFactory->create();
            $this->messageManager->addErrorMessage(__('Error. Module Disabled. '));

            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }


        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('StoreLocator_Stores::store_locator')
            ->addBreadcrumb(__('Stores'), __('List'));
        $resultPage->getConfig()->getTitle()->prepend(__('Stores'));

        return $resultPage;
    }
}
