<?php

namespace StoreLocator\Stores\Controller\Adminhtml\Allstores;

use StoreLocator\Stores\Api\StoresRepositoryInterface;
use StoreLocator\Stores\Api\Data\StoreLocatorInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends Action implements HttpPostActionInterface
{
    private StoresRepositoryInterface $storesRepository;

    public function __construct(
        Context $context,
        StoresRepositoryInterface $storesRepository
    ) {
        parent::__construct($context);
        $this->storesRepository = $storesRepository;
    }

    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $request = $this->getRequest();
        $storesId = (int)$this->getRequest()->getParam('id');

        if(!$storesId) {
            $this->messageManager->addErrorMessage(__('Error.'));
            return $resultRedirect->setPath('*/*/index');

        }

        try {
            $stores = $this->storesRepository->get($storesId);
            $this->storesRepository->delete($stores);
            $this->messageManager->addSuccessMessage(__('You deleted a store.'));

        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage(__('Cannot delete store'));

        }
        return $resultRedirect->setPath('*/*/index');
    }

}
