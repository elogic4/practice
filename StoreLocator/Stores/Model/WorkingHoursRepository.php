<?php

namespace StoreLocator\Stores\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use StoreLocator\Stores\Api\Data\WorkingHoursInterface;
use StoreLocator\Stores\Api\WorkingHoursRepositoryInterface;
use StoreLocator\Stores\Model\ResourceModel\WorkingHours as WorkingHoursResource;
use StoreLocator\Stores\Model\ResourceModel\WorkingHours\CollectionFactory as WorkingHoursCollectionFactory;
use StoreLocator\Stores\Model\WorkingHoursFactory;
class WorkingHoursRepository implements WorkingHoursRepositoryInterface
{

    protected $workingHoursResource;
    protected $workingHoursCollectionFactory;
    private WorkingHoursFactory $workingHoursFactory;

    public function __construct(
        WorkingHoursResource $workingHoursResource,
        WorkingHoursCollectionFactory $workingHoursCollectionFactory,
        WorkingHoursFactory $workingHoursFactory
    ) {
        $this->workingHoursResource = $workingHoursResource;
        $this->workingHoursCollectionFactory = $workingHoursCollectionFactory;
        $this->workingHoursFactory = $workingHoursFactory;
    }
    public function get(int $id): WorkingHoursInterface
    {
        $object = $this->workingHoursFactory->create();
        $this->workingHoursResource->load($object, $id);
        if (! $object->getId()) {
            throw new NoSuchEntityException(__('Unable to find working hour ID "%1"', $id));
        }
        return $object;

    }
    public function save(WorkingHoursInterface $workingHours): WorkingHoursInterface
    {
        $this->workingHoursResource->save($workingHours);
        return $workingHours;

    }
    public function getByStoreId(int $storeId): array
    {
        $collection = $this->workingHoursCollectionFactory->create();
        $collection->addFieldToFilter('store_id', $storeId);

        $workingHoursData = [];

        foreach ($collection as $workingHours) {

            $workingHoursData[] = $workingHours->getData();
        }

        return $workingHoursData;
    }
}
