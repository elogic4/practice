<?php

namespace StoreLocator\Stores\Model;

use Magento\Framework\Model\AbstractModel;
use StoreLocator\Stores\Api\Data\StoreLocatorInterface;


class Stores extends  AbstractModel implements StoreLocatorInterface
{
    protected function _construct()
    {
        parent::_init('StoreLocator\Stores\Model\ResourceModel\Stores');
    }

//    public function getStoreId(): int
//    {
//        // TODO: Implement getStoreId() method.
//    }
//
//    public function setStoreId(int $storeId): void
//    {
//        // TODO: Implement setStoreId() method.
//    }
    /**
     * @return mixed|null
     */
    public function getId()
    {
        return $this->_getData('store_id');
    }

    /**
     * @param $value
     * @return $this
     */
    public function setId($value)
    {
        $this->setData('store_id', $value);
        return $this;
    }

    /**
     * @return string
     */
    public function getStoreName(): string
    {
        return $this->getData(StoreLocatorInterface::STORE_NAME);
    }

    public function setStoreName(string $storeName): void
    {
        $this->setData(StoreLocatorInterface::STORE_NAME,$storeName);
    }

    /**
     * @return string|null
     */
    public function getStoreInfo(): ?string
    {
        return $this->getData(StoreLocatorInterface::STORE_INFO);
    }

    public function setStoreInfo(string $storeInfo): void
    {
        $this->setData(StoreLocatorInterface::STORE_INFO,$storeInfo);
    }

    /**
     * @return string|null
     */
    public function getStoreImage(): ?string
    {
        return $this->getData(StoreLocatorInterface::STORE_IMAGE);
    }

    public function setStoreImage(string $storeImage): void
    {
        $this->setData(StoreLocatorInterface::STORE_IMAGE,$storeImage);
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->getData(StoreLocatorInterface::ADDRESS);
    }

    public function setAddress(string $address): void
    {
        $this->setData(StoreLocatorInterface::ADDRESS,$address);
    }
//
//    public function getScheduleWork(): ?string
//    {
//        // TODO: Implement getScheduleWork() method.
//    }
//
    public function setScheduleWork(string $scheduleWork): void
    {
        $this->setData(StoreLocatorInterface::SCHEDULE_WORK,$scheduleWork);
    }

    /**
     * @return float|null
     */
    public function getLongitude(): ?float
    {
        return $this->getData(StoreLocatorInterface::LONGITUDE);
    }

    public function setLongitude(float $longitude): void
    {
        $this->setData(StoreLocatorInterface::LONGITUDE,$longitude);
    }

    /**
     * @return float|null
     */
    public function getLatitude(): ?float
    {
        return $this->getData(StoreLocatorInterface::LATITUDE);
    }

    public function setLatitude(float $latitude): void
    {
        $this->setData(StoreLocatorInterface::LATITUDE,$latitude);
    }
}
