<?php

namespace StoreLocator\Stores\Model;
use Magento\Framework\Model\AbstractModel;
use StoreLocator\Stores\Api\Data\WorkingHoursInterface;
class WorkingHours extends AbstractModel implements WorkingHoursInterface
{
    protected function _construct()
    {
        parent::_init('StoreLocator\Stores\Model\ResourceModel\WorkingHours');
    }

    public function getWorkingHoursId(): int
    {
        return $this->getData(WorkingHoursInterface::WORKING_HOURS_ID);
    }

    public function setWorkingHoursId(int $workingHoursId): void
    {
        $this->setData(WorkingHoursInterface::WORKING_HOURS_ID,$workingHoursId);
    }

    public function getStoreId(): int
    {
        return $this->getData(WorkingHoursInterface::STORE_ID);
    }

    public function setStoreId(int $storeId): void
    {
        $this->setData(WorkingHoursInterface::STORE_ID,$storeId);
    }

    public function getWorkingDays(): int
    {
        return $this->getData(WorkingHoursInterface::WORKING_DAYS);
    }

    public function setWorkingDays(int $workingDays): void
    {
        $this->setData(WorkingHoursInterface::WORKING_DAYS,$workingDays);
    }

    public function getWorkHoursFrom(): int
    {
        return $this->getData(WorkingHoursInterface::WORK_HOURS_FROM);
    }

    public function setWorkHoursFrom(int $workHoursFrom): void
    {
        $this->setData(WorkingHoursInterface::WORK_HOURS_FROM,$workHoursFrom);
    }

    public function getWorkHoursTo(): int
    {
        return $this->getData(WorkingHoursInterface::WORK_HOURS_TO);
    }

    public function setWorkHoursTo(int $workHoursTo): void
    {
        $this->setData(WorkingHoursInterface::WORK_HOURS_TO,$workHoursTo);
    }
}
