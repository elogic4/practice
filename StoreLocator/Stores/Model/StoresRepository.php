<?php

namespace StoreLocator\Stores\Model;

use StoreLocator\Stores\Api\Data\StoreLocatorInterface;
use StoreLocator\Stores\Api\StoresRepositoryInterface;
use StoreLocator\Stores\Api\StoresSearchResultInterface;
use StoreLocator\Stores\Model\ResourceModel\Stores\CollectionFactory;
use StoreLocator\Stores\Model\ResourceModel\Stores as StoresResource;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use StoreLocator\Stores\Api\StoresSearchResultInterfaceFactory;
use StoreLocator\Stores\Model\StoresFactory;


class StoresRepository implements  StoresRepositoryInterface
{

    private StoresResource $storesResource;
    private CollectionFactory $collectionFactory;
    private StoresFactory $storesFactory;
    private StoresSearchResultInterfaceFactory $searchResultInterfaceFactory;
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @param \StoreLocator\Stores\Model\StoresFactory $storesFactory
     * @param CollectionFactory $collectionFactory
     * @param StoresResource $storesResource
     * @param StoresSearchResultInterfaceFactory $searchResultInterfaceFactory
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        StoresFactory $storesFactory,
        CollectionFactory $collectionFactory,
        StoresResource  $storesResource,
        StoresSearchResultInterfaceFactory $searchResultInterfaceFactory,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->storesFactory = $storesFactory;
        $this->collectionFactory = $collectionFactory;
        $this->storesResource = $storesResource;
        $this->searchResultInterfaceFactory = $searchResultInterfaceFactory;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @param int $id
     * @return StoreLocatorInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): StoreLocatorInterface
    {
        $object = $this->storesFactory->create();
        $this->storesResource->load($object, $id);
        if (! $object->getId()) {
            throw new NoSuchEntityException(__('Unable to find entity store ID "%1"', $id));
        }
        return $object;

    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return StoresSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria = null): StoresSearchResultInterface
    {

        $collection = $this->collectionFactory->create();
        $searchCriteria = $this->searchCriteriaBuilder->create();

        if (null === $searchCriteria) {
            $searchCriteria = $this->searchCriteriaBuilder->create();
        } else {
            foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
                foreach ($filterGroup->getFilters() as $filter) {
                    $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                    $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
                }
            }
        }

        $searchResult = $this->searchResultInterfaceFactory->create();
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;


    }

    /**
     * @param StoreLocatorInterface $stores
     * @return StoreLocatorInterface
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function save(StoreLocatorInterface $stores): StoreLocatorInterface
    {
        $this->storesResource->save($stores);
        return $stores;

    }

    /**
     * @param StoreLocatorInterface $stores
     * @return bool
     * @throws StateException
     */
    public function delete(StoreLocatorInterface $stores): bool
    {
        try {
            $this->storesResource->delete($stores);
        } catch (\Exception $e) {
            throw new StateException(__('Unable to remove store #%1', $stores->getId()));
        }
        return true;

    }
    /**
     * @param int $id
     * @return bool
     * @throws NoSuchEntityException
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }

}
