<?php

namespace StoreLocator\Stores\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use StoreLocator\Stores\Api\Data\WorkingHoursInterface;
use Magento\Framework\Model\ResourceModel\Db\Context;
class WorkingHours extends AbstractDb
{
    public const TABLE_NAME = 'store_locator_working_hours';
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, WorkingHoursInterface::WORKING_HOURS_ID);
    }
}
