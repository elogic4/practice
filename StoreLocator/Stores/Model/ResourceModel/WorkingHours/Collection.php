<?php

namespace StoreLocator\Stores\Model\ResourceModel\WorkingHours;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use StoreLocator\Stores\Model\WorkingHours;
use StoreLocator\Stores\Model\ResourceModel\WorkingHours as WorkingHoursResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'working_hours_id';
    protected function _construct()
    {
        $this->_init(WorkingHours::class, WorkingHoursResource::class);
    }

}
