<?php

namespace StoreLocator\Stores\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use StoreLocator\Stores\Api\Data\StoreLocatorInterface;
use Magento\Framework\Model\ResourceModel\Db\Context;
class Stores extends AbstractDb
{
    public const TABLE_NAME = 'store_locator_stores';

    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, StoreLocatorInterface::STORE_ID);
    }

}
