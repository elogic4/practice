<?php

namespace StoreLocator\Stores\Model\ResourceModel\Stores;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use StoreLocator\Stores\Model\Stores;
use StoreLocator\Stores\Model\ResourceModel\Stores as StoresResource;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'store_id';
    protected function _construct()
    {
        $this->_init(Stores::class, StoresResource::class);
    }

}
