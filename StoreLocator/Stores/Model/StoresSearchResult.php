<?php

namespace StoreLocator\Stores\Model;

use Magento\Framework\Api\Search\SearchResult;
use StoreLocator\Stores\Api\StoresSearchResultInterface;

class StoresSearchResult extends SearchResult implements StoresSearchResultInterface
{

}
